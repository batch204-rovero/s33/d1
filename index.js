//Javascript Synchronous vs. Asynchronous
//Javascript is by default is synchronous, meaning only one statement is executed at a time




//This can be proven when a statement has an error.
//Javascript will not proceed to the next statement.

console.log("Hello World");
//Console.log("Hello Again");
//for (let i = 0; i <= 5000; i++) {
//	console.log(i)
//}
console.log("Goodbye");

//When certain statements  take a lot of time to process, this slows down our code.
//An example of this are when loops are used on a large amount of information or when fetching data from databases
//When an action will take some time to process, this results in code "blocking"
//Asychronous means that we can proccess to execute other statements, while time consuming code is running in the background

//Getting all posts

//The Fetch API allows you to asynchronously request for a resource(data)
//A "promise" is an object that represents the eventual completion(or failure) of an asynchronous function and its resulting value
//- A Javascript promise can be: 
    //- Pending (Working/Result is undefined)
    //- Fulfilled (The result is a value)
    //- Rejected (The result is an error object)
//Syntax
	//fetch( 'URL')

	console.log(fetch('https://jsonplaceholder.typicode.com/posts')
		);

	//Syntax
		//fetch('URL')
		// .then((response) => {})

	//Retrieves all post following the Rest API (retrieve, /post,GET)
	//By using the then method, we can now check for the status of the promise
	fetch('https://jsonplaceholder.typicode.com/posts')
	//The "fetch" methos will return a "promise" that resolves to be a "Response" object
	//The "then" method captures the "Response" object and returns another "promise" which will eventually be "resolve" or "rejected"
	.then(response => console.log(response.status))

	fetch('https://jsonplaceholder.typicode.com/posts')
	//Use the json method from the "Response" object to convert the data retrieved into JSON format to be used in our application
	.then((response) => response.json())
	//Print the converted JSON value from the "fetch" request
	//Using multiple"then" methods creates a "promise" chain
	.then((json) => console.log(json))

	//The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
	//Used in functions to indicate which portions of code should be waited for
	//Creates an asynchronous function

	async function fetchData() {

		//waits for the "fetch" method to complete then stores the value in the "result" varibal
		let result = await fetch('https://jsonplaceholder.typicode.com/posts')

		//Result returned by fetch is a promise
		console.log(result);
		//The returned "Response" is an object
		console.log(typeof result);
		//We cannot access the content of the "Response" by directly accessing its body property
		console.log(result.body);

		//Converts the data from the "Response" object as JSON
		let json = await result.json();
		//Print out the content of the "Response" object
		console.log(json);

	}

	fetchData();

	//Getting a specific

	//Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)

	fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response) => response.json())
	.then((json) => console.log(json));

	//Creating a post
	//Syntax
		//fetch('URL', options)
		//.then((response) => {})
		//.then((response) => {})
	//Creates a new post following the Rest API (create, /post/:id, POST)
	fetch('https://jsonplaceholder.typicode.com/posts',
		{

		//Sets the method of the "Request" Object to "POST" following REST API
			method: 'POST',
			//Sets the header data of the "Request" object to be sent to the backend
			//Specified that the conent will be in a JSON structure

			headers: {
				'Content-type': 'application/json'
			},
			//Sets the conent/body data of the "Request" object to be sent to the backend
			//JSON.stringify concerts the objects data into a stringified json

			body: JSON.stringify({
				title: 'New Post',
				body: 'Hello World!',
				userId: 1
			})

	})
	.then((response) => response.json())
	.then((json) => console.log(json))


	//Updating a post using PUT method

	//Updates a specific post following the Rest API (update,/post/:id, PUT)
	fetch('https://jsonplaceholder.typicode.com/posts/1',
	{
		method: 'PUT',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated post',
			body: 'Hello Again!',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

	//Updates a specific post following the Rest API(updated,/post/:id,Patch)

	//The difference between PUT and Patch is the number of the properties being changed
	//PATCH is used to updated the whole object
	//PUT  is used to update a single/serveral properties
	fetch('https://jsonplaceholder.typicode.com/posts/1',
	{
		method: 'PUT',
		headers: {
			'Content-type': 'application/json',
		},
		body: JSON.stringify({
			title:'Corrected post',
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

	//Delete a post

	//Deleting a specific post following the Rest API(delete,/post/:id,DELETE)
	fetch('https://jsonplaceholder.typicode.com/posts/1',
	{
		method: 'DELETE'
	});

	//Filtering posts
	//The data can be filtered by sending the userID  along with the URL
	//Information sent via the URL can be done by adding the question mark symbol (?)

	//Syntax
		//Individual Parameters
			// 'url?parameterName=value'
			//Multiple Parameters
				//'url?paraA=valueA&paramB=valueB'

	fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	.then((response) => response.json())
	.then((json) => console.log(json));


	//Retrieving nested/related comments to posts
	//Retrieving comments for a specific post following the REST API (retrieve, /posts/:id/comments, GET)
	fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((json) => console.log(json));